# Task 3: Fake News Detection

This repository contains the _dataset_, _format checker, scorer and baselines_ for the [CLEF2021-CheckThat! Task 3](https://sites.google.com/view/clef2021-checkthat/tasks/task-3-fake-news-detection). 
Given the text of a news article, determine whether the claims made in the article are true, partially true, false or other (e.g., claims in dispute) and also detect the topical domain of the article. This task will run in English.

````
TBA
````

This file contains the basic information regarding the CLEF2021-CheckThat! Task 3
on check-worthiness on tweets provided for the CLEF2021-CheckThat! Lab
on "Automatic Detecting Check-Worthy Claims, Previously Fact-Checked Claims, and Fake News".
The current version (?) corresponds to the release of the first batch of the training data set. 
The test set is released with the current version.


__Table of contents:__
- [Evaluation Results](#evaluation-results)
- [List of Versions](#list-of-versions)
- [Contents of the Task 3 Directory](#contents-of-the-repository)
- [Input Data Format](#input-data-format)
	- [Subtask 3A: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles)
	- [Subtask 3B: Topical Domain Classification of News Articles](#Subtask-3B-Topical-Domain-Classification-of-News-Articles)
- [Output Data Format](#output-data-format)
	- [Subtask 3A: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-1)
	- [Subtask 3B: Topical Domain Classification of News Articles](#Subtask-3B-Topical-Domain-Classification-of-News-Articles-1)
- [Format Checkers](#format-checkers)
	- [Subtask 3A: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-2)
	- [Subtask 3B: Topical Domain Classification of News Articles](#Subtask-3B-Topical-Domain-Classification-of-News-Articles-2)
- [Scorers](#scorers)
	- [Subtask 3A: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-3)
	- [Subtask 3B: Topical Domain Classification of News Articles](#Subtask-3B-Topical-Domain-Classification-of-News-Articles-3)
- [Evaluation Metrics](#evaluation-metrics)
- [Baselines](#baselines)
	- [Subtask 3A: Multi-Class Fake News Detection of News Articles](#Subtask-3A-Multi-Class-Fake-News-Detection-of-News-Articles-4)
	- [Subtask 3B: Topical Domain Classification of News Articles](#Subtask-3B-Topical-Domain-Classification-of-News-Articles-4)
- [Credits](#credits)

## Evaluation Results

TBA

## List of Versions


## Contents of the Task 3 Directory
We provide the following files:

* Main folder: [data](./data)
* Main folder: [baseline](./baseline)<br/>
	Contains scripts provided for baseline models of the tasks
* Main folder: [baseline](./format_checker)<br/>
	Contains scripts provided to check format of the submission file
* Main folder: [baseline](./scorer)<br/>
	Contains scripts provided to score output of the model when provided with label (i.e., dev set).

* [README.md](./README.md) <br/>
	This file!


## Input Data Format

### Subtask 3A: Multi-Class Fake News Detection of News Articles

TBA

### Topical Domain Classification of News Articles

TBA

## Output Data Format

### Subtask 3A: Multi-Class Fake News Detection of News Articles

TBA

### Topical Domain Classification of News Articles

TBA


## Format Checkers

#### Subtask 3A: Multi-Class Fake News Detection of News Articles

TBA

#### Topical Domain Classification of News Articles

TBA

## Scorers

### Subtask 3A: Multi-Class Fake News Detection of News Articles

TBA

### Topical Domain Classification of News Articles

TBA

## Evaluation Metrics
This task is evaluated as a classification task. We will use accuracy, and F1 measure.


## Baselines

### Subtask 3A: Multi-Class Fake News Detection of News Articles

TBA

### Topical Domain Classification of News Articles

TBA


## Credits

Task 3 Organizers: TBA

Task website: https://sites.google.com/view/clef2021-checkthat/tasks/task-3-fake-news-detection

Contact:   clef-factcheck@googlegroups.com
