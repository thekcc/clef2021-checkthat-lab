# CLEF2021--CheckThat-Lab
This repository contains the _dataset_, _format checker, scorer and baselines_ for the [CLEF2021-CheckThat! Lab](https://sites.google.com/view/clef2021-checkthat). 
The shared task focuses on detecting check-worthy claims, previously fact-checked claims, and fake news. 
The CLEF2021-CheckThat! Lab has three main tasks each offered in variaty of languages:
  - [Task 1: Check-Worthiness Estimation](task1)
    - Subtask 1A: Check-Worthiness of Tweets
      - **Arabic**
      - **Bulgarian**
      - **English**
      - **Spanish**
    - Subtask 1B: Check-Worthiness of Debates/Speeches
      - **English**
  - [Task 2: Detecting Previously Fact-Checked Claims](task2)
    - Subtask 2A: Detect Previously Fact-Checked Claims in Tweets
      - **Arabic**
      - **English**
    - Subtask 2B: Detect Previously Fact-Checked Claims in Political Debates/Speeches
      - **English**
  - [Task 3: Fake News Detection](task3)
    - Subtask 3A: Multi-Class Fake News Detection of News Articles
      - **English**
    - Subtask 3B: Topical Domain Classification of News Articles
      - **English**

# Licensing

These datasets are free for general research use.

# Credits

Lab Organizers:

Task website: https://sites.google.com/view/clef2021-checkthat

Contact:   clef-factcheck@googlegroups.com

# Citation

You can find the overview paper on the CLEF2021-CheckThat! Lab in the paper, "The CLEF-2021 CheckThat! Lab on Detecting Check-Worthy Claims, Previously Fact-Checked Claims, and Fake News" (see citation bellow) in this [link]().

```


```
